import Vue from 'vue'
import Vuex from 'vuex'
import TodoModule from '@/store/todo-store'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    TodoModule,
  }
})
