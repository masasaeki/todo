
import { TOGGLE_CONNECTING, GET_TODO,
  GET_TODOS, ADD_TODO, REMOVE_TODO, CHANGE_TODO,
  GET_TODO_ASYNC, GET_TODOS_ASYNC, ADD_TODO_ASYNC,
  REMOVE_TODO_ASYNC, TOGGLE_TODO_ASYNC, UPDATE_TODO_ASYNC,
  GET_TODO_BY_ID, GET_TODOS_BY_TIME } from '@/lib/mutation-types'
import fetch from 'cross-fetch'

const URL = `http://192.168.99.100:3000`
export default {
  namespaced: true,
  state: {
    connecting: false,
    todo: {
      content: '',
      due_on: '',
      finished: false,
      id: 0,
    },
    todos: [
    ]
  },
  getters: {
    timeSortedTodos(state) {
      return state.todos.slice().sort((a, b) => { 
        return Date.parse(a.due_on) > Date.parse(b.due_on) ? 1 : -1
      })
    },
    sortedTodos(state) {
      return state.todos.slice().sort((a, b) => { 
        return a.id < b.id ? 1 : -1
      })
    },
    todosCount(state) {
      return state.todos.length
    },
    notFinishedTodos(state, getters) {
      return getters.sortedTodos.filter((todo) => {
        return !todo.finished
      })
    },
    finishedTodos(state, getters) {
      return getters.sortedTodos.filter((todo) => {
        return todo.finished
      })
    },
    notFinishedTodosCount(state, getters) {
      return getters.notFinishedTodos.length
    },
    [GET_TODOS_BY_TIME](state, getters) {
      return (timeStr) => {
        let compared = Date.parse(timeStr)
        if (compared) {
          return getters.timeSortedTodos.filter((todo) => {
            return Date.parse(todo.due_on) >= compared
          })
        } else {
          compared = Date.now()
          return getters.timeSortedTodos.filter((todo) => {
            return Date.parse(todo.due_on) >= compared
          })
        }
      }
    },
    [GET_TODO_BY_ID](state) {
      return (id) => {
        let tmpTodo = state.todos.find((todo) => {
          return Number(todo.id) === Number(id)
        })
        return Object.assign({}, tmpTodo)
      }
    },
  },
  mutations: {
    [GET_TODO](state, payload) {
      state.todo = payload.todo
    },
    [GET_TODOS](state, payload) {
      state.todos = payload.todos
    },
    [ADD_TODO](state, payload) {
      // const lastId = state.todos.reduce((a, b)=> a.id > b.id ? a.id : b.id)
      // console.log(`--------lastID ${lastId}`)
      // payload.todo.id = lastId + 1
      state.todos.push(payload.todo)
    },
    [REMOVE_TODO](state, payload) {
      state.todos = state.todos.filter((todo)=>{
        return todo.id !== payload.todo.id
      })
    },
    [CHANGE_TODO](state, payload) {
      state.todos.forEach((todo)=>{
        if(todo.id === payload.todo.id) {
          todo.content = payload.todo.content
          todo.finished = payload.todo.finished
          todo.due_on = payload.todo.due_on
        }
      })
    },
    [TOGGLE_CONNECTING](state, payload) {
      state.connecting = !state.connecting 
    },
  },
  actions: {
    [GET_TODO_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      fetch(`${URL}/todos/${payload.id}`, { method: 'GET' })
      .then(response => {
        if (200 <= response.status < 300 && response.ok) {
          return response.json();
        } else {
          throw new Error(`status NG ${response.status}:${response.statusText}`);
        }
      })
      .then(todo => {
        context.commit(GET_TODO, { todo })
        // console.log(`--------------${ todo.id }`)
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
    },
    [GET_TODOS_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      fetch(`${URL}/todos`, { method: 'GET' })
      .then(response => {
        if (200 <= response.status < 300 && response.ok) {
          return response.json();
        } else {
          throw new Error(`status NG ${response.status}:${response.statusText}`);
        }
      })
      .then(todos => {
        context.commit(GET_TODOS, { todos })
        // console.log({ todos })
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
    },
    [ADD_TODO_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      fetch(`${URL}/todos`, {
        method: 'POST',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(payload.todo),
      })
      .then(response => {
        if (200 <= response.status < 300 && response.ok) {
          return response.json();
        } else if (response.status === 422) {
          return response.json();//validation error
        } else {
          throw new Error(`status NG ${response.status}:${response.statusText}`);
        }
      })
      .then(todo => {
        context.commit(ADD_TODO, {todo})
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 1000)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 1000)
      })
    },
    [REMOVE_TODO_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      fetch(`${URL}/todos/${payload.todo.id}`, {
        method: 'DELETE',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
      })
      .then(response => {
        if (200 <= response.status < 300 && response.ok) {
          return response.json();
        } else {
          throw new Error(`status NG ${response.status}:${response.statusText}`);
        }
      })
      .then(todo => {
        // console.log(todo)
        context.commit(REMOVE_TODO, payload)
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
    },
    [TOGGLE_TODO_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      const tmpTodo = Object.assign({}, payload.todo, {finished: !payload.todo.finished})
      fetch(`${URL}/todos/${payload.todo.id}`, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(tmpTodo),
      })
      .then(response => {
        if (200 <= response.status < 300 && response.ok) {
          return response.json();
        } else if (response.status === 422) {
          return response.json();//validation error
        } else {
          throw new Error(`status NG ${response.status}:${response.statusText}`);
        }
      })
      .then(todo => {
        context.commit(CHANGE_TODO, { todo })
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
    },

    [UPDATE_TODO_ASYNC](context, payload) {
      context.commit(TOGGLE_CONNECTING, payload)
      fetch(`${URL}/todos/${payload.todo.id}`, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(payload.todo),
      })
      .then(res => res.json())
      .then(todo => {
        context.commit(CHANGE_TODO, { todo })
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>{
          context.commit(TOGGLE_CONNECTING, payload)
        }, 400)
      })
    },
  }
}
