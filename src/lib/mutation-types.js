export const TODO_MODULE = 'TodoModule'

export const TOGGLE_CONNECTING = 'toggleConnecting'
export const GET_TODO = 'getTodo'
export const GET_TODOS = 'getTodos'
export const ADD_TODO = 'addTodo'
export const REMOVE_TODO = 'removeTodo'
export const CHANGE_TODO = 'changeTodo'

export const GET_TODO_ASYNC = 'getTodoAsync'
export const GET_TODOS_ASYNC = 'getTodosAsync'
export const ADD_TODO_ASYNC = 'addTodoAsync'
export const REMOVE_TODO_ASYNC = 'removeTodoAsync'
export const TOGGLE_TODO_ASYNC = 'toggleTodoAsync'
export const UPDATE_TODO_ASYNC = 'updateTodoAsync'

export const GET_TODO_BY_ID = 'getTodoById'
export const GET_TODOS_BY_TIME = 'getTodosByTime'