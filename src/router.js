import Vue from 'vue'
import Router from 'vue-router'
import Todos from './views/Todos.vue'
import Home from './views/Home.vue'
import Edit from './views/Edit.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/todos',
      name: 'todos',
      component: Todos
    },
    {
      path: '/add',
      name: 'add',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "add" */ './views/Add.vue')
    },
    {
      path: '/edit/:id(\\d)',
      name: 'edit',
      component: Edit,
      props: routes => ({
        id: Number(routes.params.id)
      })
    },
  ]
})
