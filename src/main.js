
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { ValidationProvider, ValidationObserver, localize, extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules'
import ja from 'vee-validate/dist/locale/ja.json'

// loop over all rules
for (let rule in rules) {
  extend(rule, {
    ...rules[rule],
  });
}
localize('ja', ja)
// Register it globally
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

Vue.config.productionTip = false
    
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
