module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  // options...
  devServer: {
    watchOptions: {
      ignored: /node_modules/,
      aggregateTimeout: 300,
      poll: 1000,
    },
  }
}