import { shallowMount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('タイトルが表示される', () => {
    // const msg = 'new message'
    const wrapper = shallowMount(Home, {
      // propsData: { msg }
    })
    expect(wrapper.text()).toMatch("Home")
    expect(wrapper.text()).toMatch("Todoリストでやるべきことを管理しましょう。")
  })
})
