import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import App from '@/App.vue'
import flushPromises from 'flush-promises'
import VueRouter from 'vue-router'
import router from '@/router'
import store from '@/store'


const localVue = createLocalVue()
localVue.use(VueRouter)

describe('App.vue', () => {
  it('ホームが表示される', () => {
    const wrapper = mount(App, {
      // localVue,
      router,
      store,
    })
    expect(wrapper.text()).toMatch("Home")
  })

  it('リンクをクリックするとホームページが表示される', async () => {
    const wrapper = mount(App, {
      // localVue,
      router,
      store,
    })
    // wrapper.find('router-link[to="/"]').trigger('click')
    wrapper.find('a[href="/"]').trigger('click')
    await flushPromises()
    expect(wrapper.text()).toMatch("Home")
  })

  it('リンクをクリックするとリストページが表示される', async () => {
    const wrapper = mount(App, {
      // localVue,
      router,
      store,
    })
    // wrapper.find('router-link[to="/todos"]').trigger('click')
    wrapper.find('a[href="/todos"]').trigger('click')
    await flushPromises()
    expect(wrapper.text()).toMatch("ToDo List")
  })

  it('リンクをクリックすると追加ページが表示される', async () => {
    const wrapper = mount(App, {
      // localVue,
      router,
      store,
    })
    // wrapper.find('router-link[to="/add"]').trigger('click')
    wrapper.find('a[href="/add"]').trigger('click')
    await flushPromises()
    expect(wrapper.text()).toMatch("Add")
  })
})
