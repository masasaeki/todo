import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import Add from '@/views/Add.vue'
import flushPromises from 'flush-promises'
import VueRouter from 'vue-router'
import router from '@/router'
import store from '@/store'
import { ValidationProvider, ValidationObserver, localize, extend } from 'vee-validate'

// import * as rules from 'vee-validate/dist/rules'
// min, required
// import { min, required } from 'vee-validate/dist/rules'

import ja from 'vee-validate/dist/locale/ja.json'


const localVue = createLocalVue()

// loop over all rules
// for (let rule in rules) {
// // for (let rule in [ min, required ]) {
//   extend(rule, {
//     ...rules[rule],
//   });
// }

localize('ja', ja)
// Register it globally
localVue.component('ValidationProvider', ValidationProvider)
localVue.component('ValidationObserver', ValidationObserver)

// localVue.use(VueRouter)

describe('Add.vue', () => {
  it('ホームが表示される', async () => {
    const wrapper = mount(Add, {
      localVue,
      router,
      store,
    })
    await flushPromises()
    const input = wrapper.find('input[name="content"]')
    // expect(input.element.value).toMatch("ちょっと筋トレする")
    expect(input.element).toBeTruthy()
  })

})
