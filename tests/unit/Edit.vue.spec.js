import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import Edit from '@/views/Edit.vue'
import flushPromises from 'flush-promises'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import router from '@/router'
import store from '@/store'
import { ValidationProvider, ValidationObserver, localize, extend } from 'vee-validate'

// import * as rules from 'vee-validate/dist/rules'
// min, required
// import { min, required } from 'vee-validate/dist/rules'

import ja from 'vee-validate/dist/locale/ja.json'


const localVue = createLocalVue()

// loop over all rules
// for (let rule in rules) {
//   extend(rule, {
//     ...rules[rule],
//   });
// }
localize('ja', ja)
// Register it globally
localVue.component('ValidationProvider', ValidationProvider)
localVue.component('ValidationObserver', ValidationObserver)

localVue.use(Vuex)
localVue.use(VueRouter)

describe('Edit.vue', () => {
  it('idに対応した値がフォームに表示される', async () => {
    const wrapper = mount(Edit, {
      localVue,
      router,
      store,
      propsData: { id: 1 }
    })
    // await flushPromises()
    // wrapper.setProps({ id: 2 })
    await flushPromises()
    const input = wrapper.find('input[name="content"]')
    expect(input.element.value).toMatch("ちょっと筋トレする")
    // expect(input.element.value).toMatch("3キロ走る")
  })

})
