import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import Todos from '@/views/Todos.vue'
import Vuex from 'vuex'
import TodoModule from '@/store/todo-store'
import { TOGGLE_CONNECTING, GET_TODO,
  GET_TODOS, ADD_TODO, REMOVE_TODO, CHANGE_TODO,
  GET_TODO_ASYNC, GET_TODOS_ASYNC, ADD_TODO_ASYNC,
  REMOVE_TODO_ASYNC, TOGGLE_TODO_ASYNC, UPDATE_TODO_ASYNC,
  GET_TODO_BY_ID, GET_TODOS_BY_TIME, TODO_MODULE } from '@/lib/mutation-types'
import flushPromises from 'flush-promises'
import VueRouter from 'vue-router'
import router from '@/router'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)


let state
let storeMock
let actions

beforeEach(() => {
  state = {
    connecting: false,
    todo: {
      content: '',
      due_on: '',
      finished: false,
      id: 0,
    },
    todos: [
      {
        content: '5キロ走る',
        due_on: '2019-10-20',
        finished: false,
        id: 1,
      },
      {
        content: '筋トレをする',
        due_on: '2019-10-21',
        finished: false,
        id: 2,
      },
    ]
  }
  actions = {
    [GET_TODOS_ASYNC]: jest.fn()
  }
  storeMock = new Vuex.Store({
    modules: {
      // TodoModule

      TodoModule: {
        state,
        getters: TodoModule.getters,
        mutations: TodoModule.mutations,
        actions,
      },
    }
  })
})

describe('Todos.vue', () => {
  it('タイトルが表示される', () => {
    const wrapper = shallowMount(Todos, { 
      router,
      store,
    })
    expect(wrapper.text()).toMatch("ToDo List")
  })

  it('TODO一覧を取得する', async () => {
    const wrapper = mount(Todos, { 
      router,
      store,
     })
    expect(wrapper.text()).toMatch("ToDo List")
    wrapper.find('.a').trigger('click')
    await flushPromises()
    wrapper.find('.b').trigger('click')
    await flushPromises()
    wrapper.find('.a').trigger('click')
    await flushPromises()
    expect(wrapper.html()).toMatch("ちょっと筋トレする")
    expect(wrapper.html()).toMatch("牛乳を買う")

    // expect(actions[GET_TODOS_ASYNC]).toHaveBeenCalled()
  })
})
